let hit = document.querySelectorAll(".hitO");
let resetBtn = document.querySelector("#reset");
let newBtn = document.querySelector("#new");
let msg = document.querySelector(".msg-container-hide");
let p = document.querySelector("#msg");
let d = 0;

let turn = true;
const winPatterns = [
    [0, 1, 2],
    [0, 3, 6],
    [0, 4, 8],
    [1, 4, 7],
    [2, 5, 8],
    [2, 4, 6],
    [3, 4, 5],
    [6, 7, 8]
];

const reset = () => {
    turn = true;
    enableGame();
    msg.setAttribute("class", "msg-container-hide");
    d = 0
}

hit.forEach((h) => {
    h.addEventListener("click", () => {
        if (turn) {
            h.setAttribute("class", "hitO");
            h.innerText = "O";
            turn = false;
        } else {
            h.setAttribute("class", "hitX");
            h.innerText = "X";
            turn = true;
        }
        d += 1;
        h.disabled = true;

        checkWinner(d);
    });
});

const disableGame = () => {
    for(let i of hit){
        i.disabled = true;
    }
}

const enableGame = () => {
    for(let i of hit){
        i.disabled = false;
        i.innerText = "";
    }
}

const show = (val) => {
    msg.setAttribute("class", "msg-container-show");
    p.innerText = `Congratulations, Winner is ${val}`;
    disableGame();
}

const checkWinner = (d) => {
    for(let i of winPatterns) {
        let val1 = hit[i[0]].innerText;
        let val2 = hit[i[1]].innerText;
        let val3 = hit[i[2]].innerText;

        if (val1 != "" && val2 != "" && val3 != ""){
            if(val1 === val2 && val2 === val3){
                show(val1);
            } else if(d === 9) {
                msg.setAttribute("class", "msg-container-show");
                p.innerText = `Its a DRAW`;
                disableGame();
            }
        }
    }
}

newBtn.addEventListener("click", reset);
resetBtn.addEventListener("click", reset);
