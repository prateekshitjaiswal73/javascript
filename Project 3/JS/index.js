let hasBlackJack = false
let isAlive = false
let message = ""
let cards = []
let sum = 0
let sn = 0

let m = document.getElementById("message")
let s = document.getElementById("elements")
//let c = document.getElementById("elementc")
let c = document.querySelector("#elementc")

let playerg = {
    name: "Player",
    chips: 0
}

let player = document.getElementById("player")
player.textContent = playerg.name + " : $ "+ playerg.chips

function getRandomCard() {
    let r = Math.floor(Math.random()*14)
/*    let l = {
        11 : 'el',
        12 : 'tw',
        13 : 'th'
    }
    r in l
*/
    if (r === 1) {
        return 11
    }
    else if (r > 10) {
        return 10
    }
    else {
        return r
    }
}

function startGame() {
    sn += 1
    isAlive = true
    let firstCard = getRandomCard()
    let secondCard = getRandomCard()
    cards = [firstCard , secondCard]
    sum = firstCard + secondCard
    renderGame()
}

function renderGame() {
    if (sum <= 20) {
        message = "Do you want to draw a new card?"
    }
    else if (sum === 21) {
        message = "You've got Blackjack! "
        hasBlackJack = true
        playerg["chips"] += 100
        player.textContent = playerg.name + " : $ "+ playerg.chips
    }
    else {
        message = "You're out of the game! "
        isAlive = false
    }

    m.textContent = message
    s.textContent = sum
    let content = ""
    for(let i = 0; i < cards.length; i++) {
        content += cards[i] + " "
    }
    c.textContent = content
}

function newCard() {
    if (isAlive === true && hasBlackJack === false && sn >= 1) {
        let a = "Drawing a new card from the deck! "
        let newC = getRandomCard()
        sum += newC
        cards.push(newC)
        m.textContent = a
        renderGame()
    }
    else if(sn === 0) {
        let a = "Please start the game first, to pick a new card. "
        m.textContent = a
    }
    else {
        let a = "Game Over!!! "
        m.textContent = a
    }
}

function reset() {
    m.textContent = "Want to play a round ? "
    s.textContent = ''
    c.textContent = ''
    isAlive = true
    hasBlackJack = false
    sn = 0
}

function resetwg() {
    reset()
    playerg["chips"] = 0
    player.textContent = playerg.name + " : $ "+ playerg.chips
}
