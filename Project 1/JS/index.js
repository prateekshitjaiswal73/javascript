let c = document.getElementById("count");
let r = document.getElementById("result");
let s = document.getElementById("save");
let ch = 0
let ct = 0
let sc = 0

function inc() {
    ct += 1
    ch += 1
    c.innerText = ch
}

function save() {
    sc += 1
    let m = "Total Count = " + ct
    r.innerText = m
    s.innerText += " " + ch + " -"
    c.textContent = 0
    ch = 0
}

function reset() {
    c.innerText = "0"
    r.innerText = "Start"
    s.innerText = "Previous Enteries: "
    ch = 0
    ct = 0
}
