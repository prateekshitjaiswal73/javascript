let hr = document.getElementById("hr");
let min = document.getElementById("min");
let sec = document.getElementById("sec");
const startBtn = document.getElementById("startBtn");
const stopBtn = document.getElementById("stopBtn");
const resetBtn = document.getElementById("resetBtn");

let h = 0; 
hr.textContent = "00";
let m = 0; 
min.textContent = "00";
let s = 0; 
sec.textContent = "00";

let si = null;

function increment() {
    s += 1;
    sec.textContent = s;
    if (m == 60) {
        h += 1;
        hr.textContent = h;
        m = 0;
        min.textContent = m;
    }
    if (s == 60) {
        m += 1;
        min.textContent = m;
        s = 0;
        sec.textContent = s;
    }
}

startBtn.addEventListener("click", function() {
    return si = setInterval(increment, 1000);
});

stopBtn.addEventListener("click", function() {
    clearInterval(si);
});

resetBtn.addEventListener("click", function() {
    clearInterval(si);
    h = 0; 
    hr.textContent = "00";
    m = 0; 
    min.textContent = "00";
    s = 0; 
    sec.textContent = "00";

});
